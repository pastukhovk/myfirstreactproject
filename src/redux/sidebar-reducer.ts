export type FriendType = {
    id:number
    name:string
    imageLink: string | null
}
export type InitialStateType = {
    friends: Array<FriendType>
}

let initialState:InitialStateType = {
    friends: [
        {
            id: 1,
            name: "Алексей",
            imageLink: "https://pp.userapi.com/KlAwADMZlbPA-FEmLXGnZ7k6MjlRMHJh5itsqg/Msh6C01fiIo.jpg?ava=1"
        },
        {
            id: 2,
            name: "Алина",
            imageLink: "https://sun9-58.userapi.com/c854224/v854224366/69a6/H2v6R56WHYA.jpg?ava=1"
        },
        {id: 3, name: "Михаил", imageLink: "https://cs6.pikabu.ru/avatars/1041/v1041039-815071633.jpg"},
    ]
};



export const sidebarReducer = () => (initialState);

