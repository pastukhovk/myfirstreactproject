import ReactDOM from "react-dom";
import App from "../App";
import React from "react";
import {profileReducer} from "./profile-reducer.js";
import {dialogsReducer} from "./dialogs-reducer.js";


let store = {
    _rerenderTree() {
        console.log("state changed");
    },
    _state: {
        dialogsPage: {
            MessageData: [
                {
                    id: 1,
                    author: "Дима",
                    text: "Привет как дела?",
                },
                {
                    id: 2,
                    author: "Сергей",
                    text: "Что новгого",
                },
                {
                    id: 3,
                    author: "Ваня",
                    text: "Скинь домашку",
                },
            ], newMessageText: "Введите сообщение",
            ContactData: [
                {id: 1, name: "Дима", surname: "Петухов"},
                {id: 2, name: "Петя", surname: "Свинолупов"},
                {id: 3, name: "Лёня", surname: "Степанов"},
                {id: 4, name: "Лена", surname: "Александрова"},

            ],
        },
        profilePage: {
            myPostElements: [
                {
                    id: 1,
                    text: "Это текст первого поста",
                    imageLink: "https://static-cdn.karaoke.ru/content/author/50/b3/2.16741.resize100x100.jpg"
                },
                {
                    id: 2,
                    text: "Это текст 2 поста,",
                    imageLink: "https://static-cdn.karaoke.ru/content/author/50/b3/2.16741.resize100x100.jpg"
                },
                {
                    id: 3,
                    text: "Это текст 3 поста",
                    imageLink: "https://static-cdn.karaoke.ru/content/author/50/b3/2.16741.resize100x100.jpg"
                },

            ],
            newPostText: 'Добавить новый пост',
        },
        sidebar: {
            friends: [
                {
                    id: 1,
                    name: "Алексей",
                    imageLink: "https://pp.userapi.com/KlAwADMZlbPA-FEmLXGnZ7k6MjlRMHJh5itsqg/Msh6C01fiIo.jpg?ava=1"
                },
                {
                    id: 2,
                    name: "Алина",
                    imageLink: "https://sun9-58.userapi.com/c854224/v854224366/69a6/H2v6R56WHYA.jpg?ava=1"
                },
                {id: 3, name: "Михаил", imageLink: "https://cs6.pikabu.ru/avatars/1041/v1041039-815071633.jpg"},
            ]
        },
    },
    getState() {
        return this._state;
    },
    AddPostState() {
        let textPost = this._state.profilePage.newPostText;
        let NewPost = {
            id: 5,
            text: textPost,
            imageLink: "https://static-cdn.karaoke.ru/content/author/50/b3/2.16741.resize100x100.jpg",
        };
        this._state.profilePage.myPostElements.push(NewPost);
        this._rerenderTree(this._state);
        this._state.profilePage.newPostText = ""; //обнуляем поле ввода после добавления поста


    },
    UpdateNewPostText(NewText) {

        this._state.profilePage.newPostText = NewText;
        this._rerenderTree(this._state);


    },

    addMessageText()  {
        let newMessageText = this._state.dialogsPage.newMessageText;

        let newMessage = {
            id: 4,
            author: "Дима",
            text: newMessageText,
        };
        this._state.dialogsPage.MessageData.push(newMessage);
        this._rerenderTree(this._state);
        this._state.dialogsPage.newMessageText = "";

    },
    updateNewMessageText(newMessageText) {

        this._state.dialogsPage.newMessageText = newMessageText;
        this._rerenderTree(this._state);
    },

    dispatch (action) {
        this._state.profilePage = profileReducer(this._state.profilePage, action);
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
        this._rerenderTree(this._state);


    },

    subscribe(observer) {

        this._rerenderTree = observer;
    },

};




window.store = store;
export default store;