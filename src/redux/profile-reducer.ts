import {FormAction, stopSubmit} from "redux-form";
import {profileAPI} from "../api/profile-api";
import {ResultCodesEnum} from "../api/api";
import {AppStateType, GetActionsTypes} from "./redux-store";
import {ThunkAction} from "redux-thunk";


export type MyPostElementsType = {
    id: number
    text: string
    imageLink: string
}
type ContactsType = {
    github: string | null
    vk: string | null
    facebook: string | null
    instagram: string | null
    twitter: string | null
    website: string | null
    youtube: string | null
    mainLink: string | null

}
export type PhotosType = {
    small: string | null
    large: string | null

}
export type ProfileType = {
    userId: number | null
    lookingForAJob: boolean | null
    lookingForAJobDescription: string | null
    fullName: string | null
    contacts: ContactsType | null
    photos: PhotosType | null
}

let initialState = {
    myPostElements: [
        {
            id: 1,
            text: "Это текст первого поста",
            imageLink: "https://social-network.samuraijs.com/activecontent/images/users/6588/user.jpg?v=34"
        },
        {
            id: 2,
            text: "Это текст 2 поста,",
            imageLink: "https://social-network.samuraijs.com/activecontent/images/users/6588/user.jpg?v=34"
        },
        {
            id: 3,
            text: "Это текст 3 поста",
            imageLink: "https://social-network.samuraijs.com/activecontent/images/users/6588/user.jpg?v=34"
        },

    ] as Array<MyPostElementsType>,
    newPostText: '',
    profile: null as ProfileType | null,
    isFetching: false,
    status: "hello",
};
type InitialStateType = typeof initialState
export const profileReducer = (state = initialState, action: ActionsTypes): InitialStateType => {

    switch (action.type) {
        case "SN/PROFILE/ADD-POST":
            let textPost = action.newPostText;
            let stateCopy = {...state};
            if (textPost) {
                let NewPost = {
                    id: state.myPostElements.length + 1,
                    text: textPost,
                    imageLink: "https://social-network.samuraijs.com/activecontent/images/users/6588/user.jpg?v=34",
                };

                stateCopy.myPostElements = [...state.myPostElements];
                stateCopy.myPostElements.push(NewPost);
            }
            return stateCopy;
        case "SN/PROFILE/SET-USER-PROFILE":
            return {...state, profile: action.profile};
        case "SN/PROFILE/SET-STATUS":
            return {...state, status: action.status};
        case "SN/PROFILE/SAVE-MAIN-PHOTO":
            return {...state, profile: {...state.profile, photos: action.photo} as ProfileType};

    }

    return state;

};

// ActionCreators

export const actions = {
    addPost: (text: string) => ({type: "SN/PROFILE/ADD-POST", newPostText: text} as const),
    setUserProfile: (profile: ProfileType) => ({type: "SN/PROFILE/SET-USER-PROFILE", profile} as const),
    setStatus: (status: string) => ({type: "SN/PROFILE/SET-STATUS", status} as const),
    savePhotoSuccess: (photo: PhotosType) => ({type: "SN/PROFILE/SAVE-MAIN-PHOTO", photo} as const)
}
type ActionsTypes = GetActionsTypes<typeof actions>


//ThunkCreators
type ThunkCreatorsType = ThunkAction<Promise<void>, AppStateType, unknown, ActionsTypes | FormAction> // FormAction for dispatch(stopSubmit)
export const getUserProfileInfo = (userId: number): ThunkCreatorsType => async (dispatch) => {
    let response = await profileAPI.getUserProfileInfo(userId);
    dispatch(actions.setUserProfile(response))
};
export const getStatus = (userId: number): ThunkCreatorsType => async (dispatch) => {
    let response = await profileAPI.getUserStatus(userId);
    dispatch(actions.setStatus(response))

};
export const updateStatus = (status: string): ThunkCreatorsType => async (dispatch) => {
    let response = await profileAPI.updateUserStatus(status);
    if (response.resultCode === ResultCodesEnum.Success)
        dispatch(actions.setStatus(status))

};

export const savePhoto = (photo: PhotosType): ThunkCreatorsType => async (dispatch) => {
    let response = await profileAPI.putMainPhoto(photo);
    if (response.resultCode === ResultCodesEnum.Success)
        dispatch(actions.savePhotoSuccess(response.data.photos))
};
export const saveProfile = (profileInfo: ProfileType): ThunkCreatorsType => async (dispatch, getState) => {
    const userId = getState().auth.userId;
    let response = await profileAPI.putProfileInfo(profileInfo);
    if (response.data.resultCode === ResultCodesEnum.Success)
        userId && dispatch(getUserProfileInfo(userId));
    else {
        let action = stopSubmit("ProfileDataForm", {_error: response.data.messages.length > 0 ? response.data.messages : "some error"});
        dispatch(action);
        return Promise.reject(response.data.messages);
    }
};
