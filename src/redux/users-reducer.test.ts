import {actions, followThunkCreator, InitialStateType, usersReducer} from "./users-reducer";
import {followAPI} from "../api/follow-api";
import {APIResponseType, ResultCodesEnum} from "../api/api";

let state: InitialStateType

beforeEach(() => {
    state = {
        users: [
            {
                id: 0,
                followed: false,
                name: "Ivan",
                photos: {
                    large: null,
                    small: null
                },
                status: "st 0"
            },
            {
                id: 1,
                followed: false,
                name: "Ivan1",
                photos: {
                    large: null,
                    small: null
                },
                status: "st 1"
            },
            {
                id: 2,
                followed: true,
                name: "Ivan2",
                photos: {
                    large: null,
                    small: null
                },
                status: "st 2"
            },

        ],
        pageSize: 20,
        totalUsersCount: 19,
        currentPage: 1,
        isFetching: false,
        followingIsProgress: [] as Array<number> //number = idUsers
    }
})

test('follow success', () => {
    const newState = usersReducer(state, actions.follow(1))
    expect(newState.users[1].followed).toBeTruthy()
})

test("unfollow success", () => {
    const newState = usersReducer(state, actions.unfollow(2))
    expect(newState.users[2].followed).toBeFalsy()
})

jest.mock('../api/follow-api') // return fake FollowAPI
const followAPIMock = followAPI as jest.Mocked<typeof followAPI>;
const result: APIResponseType = {
    data: {},
    messages: [],
    resultCode: ResultCodesEnum.Success
}

followAPIMock.follow.mockReturnValue(Promise.resolve(result))

test("follow thunk success", async () => {
    const thunk = followThunkCreator(1); // return thunk
    const dispatchMock = jest.fn(); // fake function
    const getStateMock = jest.fn(); // fake function
    await thunk(dispatchMock, getStateMock, {})
    expect(dispatchMock).toBeCalledTimes(3)
    expect(dispatchMock).toHaveBeenNthCalledWith(1, actions.toogleFollowingIsProgress(true, 1))
    expect(dispatchMock).toHaveBeenNthCalledWith(2, actions.follow(1))
    expect(dispatchMock).toHaveBeenNthCalledWith(3, actions.toogleFollowingIsProgress(false, 1))
})