import {PhotosType} from "./profile-reducer";
import {Dispatch} from "redux";
import {ThunkAction} from "redux-thunk";
import {AppStateType, GetActionsTypes} from "./redux-store";
import {usersAPI} from "../api/users-api";
import {followAPI} from "../api/follow-api";
import {ResultCodesEnum} from "../api/api";


export type UserType = {
    id: number
    name: string
    photos: PhotosType | null
    status: string | null
    followed: boolean
}

export type UsersFilterType = {
    term: string
    friend: null | boolean
}


let initialState = {
    users: [] as Array<UserType>,
    pageSize: 20,
    totalUsersCount: 19,
    currentPage: 1,
    isFetching: false,
    followingIsProgress: [] as Array<number>,//number = idUsers
    userFilter: {
        term: "",
        friend: null as null | boolean
    }
};
export type InitialStateType = typeof initialState;
export const usersReducer = (state = initialState, action: ActionsTypes): InitialStateType => {

    switch (action.type) {
        case "SN/USERS/FOLLOW":
            return {
                ...state,
                users: state.users.map(u => {

                    if (u.id === action.userId) {
                        return {...u, followed: true};
                    }
                    return u;
                })
            };
        case "SN/USERS/UNFOLLOW":
            return {
                ...state, users: state.users.map(u => {
                    if (u.id === action.userId) {
                        return {...u, followed: false};
                    }
                    return u;
                })
            };

        case "SN/USERS/SET_USERS":
            return {
                ...state, users: action.users, totalUsersCount: action.totalUsersCount
            };

        case "SN/USERS/SET_CURRENT_PAGE":
            return {
                ...state, currentPage: action.currentPage
            };
        case "SN/USERS/TOGGLE_IS_FETCHING":
            return {
                ...state, isFetching: action.isFetching
            };
        case "SN/USERS/SET_USERS_FILTER":
            return {
                ...state, userFilter: action.filter
            };
        case "SN/USERS/FOLLOWING_IS_PROGRESS":

            return {
                ...state,
                followingIsProgress: action.isFetching
                    ? [...state.followingIsProgress, action.userId]
                    : state.followingIsProgress.filter(id => id !== action.userId)

            };


        default:
            return state;
    }
};

//action creators (AC)
export const actions = {
    follow: (userId: number) => ({type: "SN/USERS/FOLLOW", userId: userId} as const),
    unfollow: (userId: number) => ({type: "SN/USERS/UNFOLLOW", userId: userId} as const),
    setUsersFilter: (filter: UsersFilterType) => ({type: "SN/USERS/SET_USERS_FILTER", filter} as const),
    setUsers: (users: Array<UserType>, totalUsersCount: number) => ({
        type: "SN/USERS/SET_USERS",
        users,
        totalUsersCount
    } as const),
    setCurrentPage: (currentPage: number) => ({
        type: "SN/USERS/SET_CURRENT_PAGE",
        currentPage
    } as const),
    toogleIsFetching: (isFetching: boolean) => ({
        type: "SN/USERS/TOGGLE_IS_FETCHING",
        isFetching
    } as const),
    toogleFollowingIsProgress: (isFetching: boolean, userId: number) => ({
        type: "SN/USERS/FOLLOWING_IS_PROGRESS",
        isFetching,
        userId
    } as const)
}
type ActionsTypes = GetActionsTypes<typeof actions>

// Thunk Creators
// 1 method
export const getUsersThunkCreator = (pageSize: number, currentPage = 1, filter: UsersFilterType) => async (dispatch: Dispatch<ActionsTypes>) => {
    dispatch(actions.toogleIsFetching(true));
    dispatch(actions.setCurrentPage(currentPage));
    dispatch(actions.setUsersFilter(filter));

    let response = await usersAPI.getUsers(pageSize, currentPage, filter.term, filter.friend);
    dispatch(actions.toogleIsFetching(false));
    dispatch(actions.setUsers(response.items, response.totalCount));
};
// 2 method
type ThunkType = ThunkAction<Promise<void>, AppStateType, unknown, ActionsTypes>
export const followThunkCreator = (userId: number): ThunkType => async (dispatch) => {
    dispatch(actions.toogleFollowingIsProgress(true, userId));
    let response = await followAPI.follow(userId);
    if (response.resultCode === ResultCodesEnum.Success) {
        dispatch(actions.follow(userId));
    }
    if (response.resultCode === 1) {
        alert("Вы не можете подписаться сами на себя");
    }
    dispatch(actions.toogleFollowingIsProgress(false, userId));
};
export const unfollowThunkCreator = (userId: number): ThunkType => async (dispatch) => {
    dispatch(actions.toogleFollowingIsProgress(true, userId));
    let response = await followAPI.unfollow(userId)
    if (response.resultCode === 0) {
        dispatch(actions.unfollow(userId));
    }
    dispatch(actions.toogleFollowingIsProgress(false, userId));

};