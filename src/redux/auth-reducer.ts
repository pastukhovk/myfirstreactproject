import {ResultCodeForCaptcha, ResultCodesEnum} from "../api/api";
import {stopSubmit} from "redux-form";
import {ThunkAction} from "redux-thunk";
import {AppStateType, GetActionsTypes} from "./redux-store";
import {authAPI} from "../api/auth-api";


type InitialStateType = {
    userId: null | number
    login: null | string
    email: null | string
    isAuth: boolean
    captchaUrl: null | string
};
let initialState: InitialStateType = {
    userId: null,
    login: null,
    email: null,
    isAuth: false,
    captchaUrl: null,
};

export const authReducer = (state = initialState, action: ActionsTypes): InitialStateType => {

    switch (action.type) {
        case "SN/AUTH/SET_USER_DATA":
        case "SN/AUTH/GET_CAPTCHA_URL":
            return {
                ...state,
                ...action.data,
            };

        default:
            return state;
    }
};

// ActionCreators

const actions = {
    setUserData: (userId: number | null, login: string | null, email: string | null, isAuth: boolean) => ({
        type: "SN/AUTH/SET_USER_DATA",
        data: {userId, login, email, isAuth}
    } as const),
    getCaptchaUrlSuccess: (captchaUrl: string | null) => ({
        type: "SN/AUTH/GET_CAPTCHA_URL",
        data: {captchaUrl}
    } as const)

}

type ActionsTypes = GetActionsTypes<typeof actions>


// ThunkCreators

type ThunkActionType = ThunkAction<Promise<void>, AppStateType, unknown, ActionsTypes>
export const userAuthorized = (): ThunkActionType => async (dispatch) => {
    let response = await authAPI.isUserAuthorized();
    if (response.resultCode === ResultCodesEnum.Success) {
        dispatch(actions.setUserData(response.data.id, response.data.login, response.data.email, true));
    }

};
export const loginThunkCreator = (email: string, password: string, rememberMe: boolean, captchaUrl: string | null): ThunkActionType => async (dispatch) => {
    let response = await authAPI.login(email, password, rememberMe, captchaUrl);
    if (response.resultCode === ResultCodesEnum.Success) {
        dispatch(userAuthorized());
        if (captchaUrl) dispatch(actions.getCaptchaUrlSuccess(null)); //обнуляем каптчу если она была ранее
    } else {
        if (response.resultCode === ResultCodeForCaptcha.CaptchaIsRequired) {
            dispatch(getCaptchaURL());
        }

         // @ts-ignore
        dispatch(stopSubmit("login", {_error: response.messages.length > 0 ? response.messages : "some error"}));
    }

};

export const logoutThunkCreator = (): ThunkActionType => async (dispatch) => {
    let response = await authAPI.logout();
    if (response.data.resultCode === ResultCodesEnum.Success) {
        dispatch(actions.setUserData(null, null, null, false));
    }
};

export const getCaptchaURL = (): ThunkActionType => async (dispatch) => {
    const captchaUrl = await authAPI.getCaptchaURL();
    dispatch(actions.getCaptchaUrlSuccess(captchaUrl.url));


};
