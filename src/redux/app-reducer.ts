
import {userAuthorized} from "./auth-reducer";
import {AppStateType, GetActionsTypes} from "./redux-store";
import {ThunkAction} from "redux-thunk";



type InitialStateType = {
    initialized:boolean
};
let initialState: InitialStateType = {
    initialized:false
};

export const appReducer = (state = initialState, action:ActionsTypes):InitialStateType => {

    switch (action.type) {
        case "SN/APP/INITIALIZED_SUCCESS":
            return {
                ...state,
                initialized:true
            };

        default:
            return state;
    }
};

 const actions = {
     initialized : () => ({type: "SN/APP/INITIALIZED_SUCCESS"} as const)
 }

type ActionsTypes =  GetActionsTypes<typeof actions>

export const initializedThunk = (): ThunkAction<void, AppStateType, unknown, ActionsTypes> => async dispatch => {
   await dispatch(userAuthorized());
     dispatch(actions.initialized())
};