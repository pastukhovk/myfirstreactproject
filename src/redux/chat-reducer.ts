import {ThunkAction} from "redux-thunk";
import {AppStateType, GetActionsTypes} from "./redux-store";
import {chatAPI, ChatMessageType} from "../api/chat-api";
import {Dispatch} from "redux";


let initialState = {
    messages:[] as ChatMessageType[]
};

export const chatReducer = (state = initialState, action: ActionsTypes) => {

    switch (action.type) {
        case "SN/CHAT/RECEIVED":
            return {
                ...state,
                messages:[...state.messages, ...action.data]
            };

        default:
            return state;
    }
};

// ActionCreators

const actions = {
   messagesReceived: (messages: ChatMessageType[])=> ({
     type: "SN/CHAT/RECEIVED",
     data: messages,
   } as const),
}

type ActionsTypes = GetActionsTypes<typeof actions>


// ThunkCreators

type ThunkActionType = ThunkAction<Promise<void>, AppStateType, unknown, ActionsTypes>

let _newMessageHandler:((messages:ChatMessageType[]) => void) | null = null

const newMessageHandlerCreator = (dispatch:Dispatch) =>  {
    if (_newMessageHandler === null) {
        _newMessageHandler = (messages) => dispatch(actions.messagesReceived(messages))
    }
    return _newMessageHandler
}
export const startMessagesListening = (): ThunkActionType => async (dispatch) => {
    chatAPI.start()
    chatAPI.subscribe(newMessageHandlerCreator(dispatch))

};
export const stopMessagesListening = (): ThunkActionType => async (dispatch) => {
    chatAPI.unsubscribe(newMessageHandlerCreator(dispatch))
    chatAPI.stop()
};
export const sendMessage  = (message:string): ThunkActionType => async (dispatch) => {
    chatAPI.sendMessage(message)
}
