import {GetActionsTypes} from "./redux-store";

type MessageDataType = {
    id: number
    author: string
    text: string
}
type ContactDataType = {
    id: number
    name: string
    surname: string
}
let initialState = {
    MessageData: [
        {
            id: 1,
            author: "Дима",
            text: "Привет как дела?",
        },
        {
            id: 2,
            author: "Сергей",
            text: "Что новгого",
        },
        {
            id: 3,
            author: "Ваня",
            text: "Скинь домашку",
        },
    ] as Array<MessageDataType>,
    ContactData: [
        {id: 1, name: "Дима", surname: "Петухов"},
        {id: 2, name: "Петя", surname: "Свинолупов"},
        {id: 3, name: "Лёня", surname: "Степанов"},
        {id: 4, name: "Лена", surname: "Александрова"},

    ] as Array<ContactDataType>,
};
export type InitialStateDialogPageType = typeof initialState;
export const dialogsReducer = (state = initialState, action: ActionsTypes): InitialStateDialogPageType => {

    if (action.type === "SN/DIALOGS/ADD-MESSAGE") {
        let newMessageText = action.newMessageText;
        let newMessage = {
            id: Date.now(),
            author: "Дима",
            text: newMessageText,
        };
        let stateCopy = {...state};
        stateCopy.MessageData = [...state.MessageData];
        stateCopy.MessageData.push(newMessage);
        return stateCopy;
    } else {
        return state;
    }
};

export const actions = {
    addMessage: (text: string) => ({type: "SN/DIALOGS/ADD-MESSAGE", newMessageText: text} as const)
}

type ActionsTypes = GetActionsTypes<typeof actions>
