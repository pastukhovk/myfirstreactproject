// типы подписчиков
type SubscriberType = (messages:ChatMessageType[])=> void
export type ChatMessageType = {
    message: string,
    photo: string,
    userId: number,
    userName: string
}
// массив подписчиков
let subscribers = [] as SubscriberType[]

let ws: WebSocket | null = null

//обработчик закрытия сокета
const closeHandler = () => {
    console.log('socket is close');
    setTimeout(createChannel, 5000)
}
// парсинг сообщений
const messageHandler = (e: MessageEvent) => {
    let newMessages = JSON.parse(e.data);
    subscribers.forEach(s=>s(newMessages))
};

// функция создания канала
function createChannel() {
    ws?.removeEventListener('close', closeHandler)
    ws?.close()
    ws = new WebSocket('wss://social-network.samuraijs.com/handlers/ChatHandler.ashx')
    ws.addEventListener('close', closeHandler)
    ws.addEventListener('message', messageHandler)
}

export const chatAPI = {
    // создание канала
    start() {
        createChannel()
    },
    // подписаться на новые сообщения
    subscribe (callback: SubscriberType) {
        subscribers.push(callback)
    },
    unsubscribe (callback: SubscriberType) {
        subscribers = subscribers.filter(s=> s!==callback)
    },
    sendMessage (message:string) {
        ws?.send(message)
    },
    stop () {
        subscribers = []
        ws?.removeEventListener('close', closeHandler)
        ws?.removeEventListener('message', messageHandler)
        ws?.close()
    }
}