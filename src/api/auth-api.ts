import {instance, APIResponseType, ResultCodeForCaptcha, ResultCodesEnum} from "./api";

type isUserAuthorizedDataType = {
    id: number, email: string, login: string
}
type LoginDataType = {
    userId: number
}
type CaptchaDataType = {
    url: string
}
export const authAPI = {
    isUserAuthorized() {
        return instance.get<APIResponseType<isUserAuthorizedDataType>>(`auth/me`).then(res => res.data)
    },

    login(email: string, password: string, rememberMe = false, captcha: null | string = null) {
        return instance.post<APIResponseType<LoginDataType, ResultCodesEnum | ResultCodeForCaptcha>>(`auth/login`, {
            email,
            password,
            rememberMe,
            captcha
        }).then(res => res.data)
    },
    logout() {
        return instance.delete(`auth/login`)
    },


    getCaptchaURL() {
        return instance.get<CaptchaDataType>(`security/get-captcha-url`).then(res => res.data)
    }

};