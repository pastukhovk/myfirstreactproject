import {instance} from "./api";
import {UserType} from "../redux/users-reducer";

type getUsersResponseType = {
    items: Array<UserType>
    totalCount: number
    error: string
}

export const usersAPI = {
    getUsers(pageSize: number, currentPage: number, term:string = "", friend:null | boolean = null) {
        return instance.get<getUsersResponseType>(`users/?count=${pageSize}&page=${currentPage}&term=${term}` + (friend === null ? '' : `&friend=${friend}`)).then(response => response.data)
    }
};