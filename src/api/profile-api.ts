import {PhotosType, ProfileType} from "../redux/profile-reducer";
import {instance, APIResponseType} from "./api";

type SavePhotoResponseType = {
    photos: PhotosType
}

export const profileAPI = {
    getUserProfileInfo(userId: number) {
        return instance.get<ProfileType>(`/profile/` + userId).then(res => res.data)
    },
    getUserStatus(userId: number) {
        return instance.get<string>(`/profile/status/` + userId).then(res => res.data)
    },
    updateUserStatus(status: string) {
        return instance.put<APIResponseType>(`/profile/status/`, {status: status}).then(res => res.data)
    },
    putMainPhoto(photoFile: any) {
        const formData = new FormData();
        formData.append("image", photoFile);
        return instance.put<APIResponseType<SavePhotoResponseType>>(`profile/photo/`, formData, {
            headers: {
                "Content-Type": "multipart/form-data"
            }
        }).then(res => res.data);
    },

    putProfileInfo(profile: ProfileType) {
        return instance.put<APIResponseType>(`/profile/`, profile)
    }
};