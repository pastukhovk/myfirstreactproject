import axios from 'axios';

export const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        "API-KEY": "cc776329-3b85-4c0c-ad2a-4be21629e03d",
    }
});


export enum ResultCodesEnum  {
    Success = 0,
    Error = 1
}
export enum ResultCodeForCaptcha  {
    CaptchaIsRequired = 10
}


export type APIResponseType<D = {}, RC = ResultCodesEnum> = {
    data: D
    resultCode: RC
    messages: Array<string>
}