import React, {lazy} from 'react';
import './App.css';
import Navbar from './components/Navbar/Navbar'
import {BrowserRouter, Route} from "react-router-dom";
import DialogsContainer from "./components/Dialogs/DialogsContainer";
import ProfileContainer from "./components/Profile/ProfileContainer";
import HeaderContainer from "./components/Header/HeaderContainer";
import {LoginContainer} from "./components/Login/Login";
import {connect, Provider} from "react-redux";
import {initializedThunk} from "./redux/app-reducer";
import Preloader from "./components/common/Preloader/Preloader";
import store, {AppStateType} from "./redux/redux-store";
import {Users} from "./components/Users/Users";
import {withSuspense} from "./hoc/withSuspense";


const ChatPage = lazy(() => import("./pages/Chat/ChatPage"));
const ChatPageSuspense = withSuspense(ChatPage)

type MapStateToPropsType = ReturnType<typeof mapStateToProps>
type MapDispatchToPropsType = {
    initializedThunk:()=>void
}
class App extends React.Component<MapStateToPropsType & MapDispatchToPropsType> {
    catchAllUnhandledErrors = (e:PromiseRejectionEvent) => {
        alert("Some error occured");
    };

    componentDidMount() {
        this.props.initializedThunk();
        window.addEventListener("unhandledrejection", this.catchAllUnhandledErrors);
    }

    componentWillUnmount() {
        window.removeEventListener("unhandledrejection", this.catchAllUnhandledErrors);
    }

    render() {
        if (!this.props.initialized) return <Preloader/>
        return (


            <div className="app-wrapper">
                <HeaderContainer />
                <Navbar />
                <div className="content">

                    <Route path='/dialogs' render={() => <DialogsContainer />}/>
                    <Route path='/profile/:userId?'
                           render={() => <ProfileContainer />}/>
                    <Route path='/users' render={() => <Users />}/>
                    <Route path='/login' render={() => <LoginContainer />}/>
                    <Route path='/chat' render={() => <ChatPageSuspense />}/>

                </div>
            </div>

        );
    }
}

let mapStateToProps = (state:AppStateType) => ({initialized: state.app.initialized});
const AppContainer = connect(mapStateToProps, {initializedThunk})(App);

const SocialFinder = () => {
    return (
        <BrowserRouter>
            <Provider store={store}>
                <AppContainer ></AppContainer>
            </Provider>
        </BrowserRouter>

    )
}

export default SocialFinder