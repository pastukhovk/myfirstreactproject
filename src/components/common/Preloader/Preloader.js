import React from 'react';
import preloader from './../../../assets/images/preloader.svg';
import styles from './preloader.module.scss'

let Preloader = (props) => {

   return (
    <div className={styles.preloader}>
    <img src = {preloader} alt="preloader" />
    </div>
   ) };

export default Preloader;

