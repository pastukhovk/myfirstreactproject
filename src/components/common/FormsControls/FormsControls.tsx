import React from "react";
import styles from './FormControls.module.scss';
import {Field, WrappedFieldProps} from "redux-form";
import {FieldValidatorType} from "../../../helpers/validators";


export const FormsControlsCreator = (Element:any):React.FC<WrappedFieldProps> => ({input, meta, ...props}) => {
    return (
        <div>
            <div>
                <Element className={(meta.touched && meta.error) ? styles.error : null} {...input} {...props}/>
            </div>
            <div>
                <span>{meta.touched ? meta.error : null}</span>
            </div>
        </div>
    )
};

export const createField = (component: string | React.Component | React.FC,
                            name:string, type:string, placeholder:string, validators:Array<FieldValidatorType>, props = {}) => {
    return (
        <Field component={component} validate={validators} type={type} name={name}
               placeholder={placeholder} {...props}/>
    )
};
