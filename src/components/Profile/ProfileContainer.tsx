import React, {useEffect} from 'react';
import Profile from "./Profile";
import {connect} from "react-redux";
import {
    getStatus,
    getUserProfileInfo, PhotosType, ProfileType,
    savePhoto,
    saveProfile,
    updateStatus
} from "../../redux/profile-reducer";
import {withRouter} from "react-router-dom";
import {withAuthRedirect} from "../../hoc/withAuthRedirect";
import {compose} from "redux";
import {AppStateType} from "../../redux/redux-store";


type PropsType = {
    match:any
    authorisedUserId:number
    history:Array<string>
    getUserProfileInfo: (userId:number) => void
    getStatus: (userId:number) => void
    profile:ProfileType
    savePhoto: (photo:PhotosType) => void
    saveProfile: (profileInfo:ProfileType) =>Promise<any>
    status: string
}
const ProfileContainer:React.FC<PropsType> = (props) => {
    const refreshProfile = () => {
        let userId = props.match.params.userId;
        if (!userId) {
            userId = props.authorisedUserId;
            if (!userId) {
                props.history.push("/login")
            }
        }
        props.getUserProfileInfo(userId);
        props.getStatus(userId);
    }

    useEffect(()=> {
        refreshProfile()
    },[props.match.params.userId]);
        return (
            <div>
                <Profile isOwner={!props.match.params.userId} {...props} profile={props.profile}
                         savePhoto={props.savePhoto}
                         saveProfile={props.saveProfile}
                         status={props.status}/>
            </div>

        )
}

let mapStateToProps = (state:AppStateType) => ({
    profile: state.profilePage.profile,
    status: state.profilePage.status,
    authorisedUserId: state.auth.userId,
    isAuth: state.auth.isAuth
});

export default compose<React.ComponentType>(
    connect(mapStateToProps, { getUserProfileInfo, getStatus, updateStatus, savePhoto, saveProfile}),
    withRouter,
     withAuthRedirect
)(ProfileContainer);
