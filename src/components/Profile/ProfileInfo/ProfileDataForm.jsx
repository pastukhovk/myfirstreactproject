import React from "react";
import {createField} from "../../common/FormsControls/FormsControls";
import {reduxForm} from "redux-form";
import styles from "../../common/FormsControls/FormControls.module.scss";

const ProfileDataForm = ({handleSubmit, profile, ...props}) => {
    return (
        <form onSubmit={handleSubmit}>
               <div> <label> Ищите работу?<br/>
            {createField("input","lookingForAJob", "checkbox","lookingForAJob",[])}
                </label></div>

            <div><label> Информация обо мне:<br/>
                {createField("textarea","aboutMe", "text","aboutMe",[])}
            </label>
            </div><div><label> Опишите Ваши умения:<br/>
                {createField("textarea","lookingForAJobDescription", "text","Your skills ",[])}
            </label></div>
            <div><label> Ваше имя:<br/>
                {createField("textarea","fullName", "text","Your name ",[])}
            </label></div>

            {Object.keys(profile.contacts).map(key => {
                return <div> <b>{key}</b>:
                    {createField("input",`contacts.${key}`, "text",key,[])}
                </div>
            })}
            { props.error &&
            <div className={styles.formError}>{props.error}</div>
            }
<button>Save</button>
        </form>
    )
};
const ProfileDataReduxForm = reduxForm({form:"ProfileDataForm"})(ProfileDataForm);
export default ProfileDataReduxForm;