import React, {useState} from 'react';
import './ProfileInfo.scss'
import Preloader from "../../common/Preloader/Preloader";
import ProfileStatusWithHooks from "./ProfileStatusWithHooks";
import userDefaultPicture from "./../../../assets/images/userPhotoDefault.jpg";
import ProfileDataReduxForm from "./ProfileDataForm";

const ProfileInfo = (props) => {

    let [editMode, setEditMode] = useState(false);
    if (!props.profile) {
        return <Preloader/>
    }

    const onMainAvatarSelected = (e) => {
        if (e.target.files.length > 0) {
            props.savePhoto(e.target.files[0]);
        }
    };
    const onSubmitForm = (data) => {
        props.saveProfile(data).then(
            () => {
                setEditMode(false);
            }
        );

    };
    return (
        <div className="profileinfo">
            <div>{props.profile.fullName}</div>
            <img className="profilePhoto" alt="profilePhoto" src={props.profile.photos.large || userDefaultPicture}/>
            <div>{props.isOwner && <input type='file' onChange={onMainAvatarSelected}/>}</div>

            <ProfileStatusWithHooks updateStatus={props.updateStatus} status={props.status}/>

            {editMode ?
                <ProfileDataReduxForm initialValues={props.profile} profile={props.profile} onSubmit={onSubmitForm}/> :
                <ProfileData profile={props.profile} isOwner={props.isOwner} goToEditMode={() => setEditMode(true)}/>
            }

        </div>
    )
}


const Contacts = ({title, value}) => <div><b>{title}:</b> {value} </div>;
const ProfileData = ({profile, isOwner, goToEditMode}) => {
    return <div>
        {isOwner && <button className="btn" onClick={goToEditMode}>Изменить информацию</button>}
        <div>{profile.lookingForAJob ? "В поисках работы" : "Работу не ищу"}</div>
        <div>Мои скилы: {profile.lookingForAJobDescription ? profile.lookingForAJobDescription : "Пока учусь"}</div>
        <span> Обо мне: {profile.aboutMe || 'информация не заполнена'} </span>
        <div>
            {Object.keys(profile.contacts).map(key => {
                return <Contacts key={key} title={key} value={profile.contacts[key]}/>
            })}

        </div>
        <div>Возраст: 26</div>
    </div>
}
export default ProfileInfo;