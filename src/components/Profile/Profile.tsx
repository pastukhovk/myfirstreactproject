import React from 'react';
import './profile.scss'
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import MypostsContainer from "./Myposts/MypostsContainer";


const Profile = (props:any) => {
    return (
        <div className="profile">
            <ProfileInfo {...props}  />

            <MypostsContainer
                store = {props.store}
            />
        </div>
    )
};

export default Profile;