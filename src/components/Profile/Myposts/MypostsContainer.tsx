/**
 * Created by Кирилл on 08.02.2020.
 */
import Myposts from "./Myposts";
import {connect} from "react-redux";
import {actions, MyPostElementsType} from "../../../redux/profile-reducer";
import {AppStateType, RootReducerType} from "../../../redux/redux-store";


let mapStateToProps = (state: AppStateType) => {
    return {
        myPostElements: state.profilePage.myPostElements,
        newPostText: state.profilePage.newPostText
    }
};

type OwnPropsType = {
    store:RootReducerType
}
export type MyPostMapStateToPropsType = {
    myPostElements: Array<MyPostElementsType>
    newPostText: string
}

export type MyPostMapDispatchToPropsType = {
    addPost: (text: string) => void
}
const MypostsContainer = connect<MyPostMapStateToPropsType, MyPostMapDispatchToPropsType, OwnPropsType, AppStateType>(mapStateToProps, {addPost:actions.addPost})(Myposts);

export default MypostsContainer;