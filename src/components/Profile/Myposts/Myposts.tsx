import React from 'react';
import './myposts.scss';
import Post from "./Post/Post";
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import {maxLength, required} from "../../../helpers/validators";
import {FormsControlsCreator} from "../../common/FormsControls/FormsControls";
import {MyPostMapDispatchToPropsType, MyPostMapStateToPropsType} from "./MypostsContainer";


const maxLength15 = maxLength(15);
const textAreaFormControls = FormsControlsCreator("textarea");
const MyPostForm:React.FC<InjectedFormProps<AddPostType>> = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field component={textAreaFormControls} type="text" validate={[required, maxLength15]}
                       name="newPostText" placeholder="Enter your post"/>
            </div>
            <div>
                <button type={"submit"}>Add post</button>
            </div>
        </form>
    )
};
const MyPostFormRedux = reduxForm<AddPostType>({form: 'userPost'})(MyPostForm);

type AddPostType = {
    newPostText:string
}

const Myposts = (props: MyPostMapStateToPropsType & MyPostMapDispatchToPropsType) => {

    let postElement = props.myPostElements.map(myPost => <Post key={myPost.id} imageLink={myPost.imageLink}
                                                               text={myPost.text}/>);

    let addPostFunc = (data:AddPostType) => {

        props.addPost(data.newPostText);
    };
    return (

        <div className="myposts">
            <h3>My Post </h3>
            <MyPostFormRedux onSubmit={addPostFunc}/>
            <div className="elements">
                {postElement.reverse()}
            </div>
        </div>
    )
};

export default Myposts;