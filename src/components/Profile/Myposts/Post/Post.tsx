/**
 * Created by Кирилл on 08.02.2020.
 */
import React from 'react';
import './post.scss';

type PostPropsType = {
    imageLink:string
    text:string
}
const Post:React.FC<PostPropsType> = (props) => {
    return (
        <div className="post">
          <img src={props.imageLink} alt="postImage" width="80" height="80" />
            {props.text}
        </div>
    )
};

export default Post;