import React from 'react';
import './users.scss';
import Preloader from "../common/Preloader/Preloader";
import {Paginator} from "../common/Paginator/Paginator";
import User from "./User";


class Users extends React.Component {

    componentDidMount() {
        this.props.getUsers(this.props.pageSize, this.props.currentPage);
    }

    onPageChanged(pageNumber) {
        this.props.getUsers(this.props.pageSize, pageNumber);
    };

    render() {
        return (

            <div className="users">
                {this.props.isFetching ? <Preloader/> : null}
                <h3>Hi my users</h3>
                {
                    this.props.users.map(u =>
                        <User key={u.id} user={u} followingIsProgress={this.props.followingIsProgress}
                              follow={this.props.follow} unfollow={this.props.unfollow}
                        />
                    )}
                <Paginator totalUsersCount={this.props.totalUsersCount} pageSize={this.props.pageSize}
                           onPageChanged={this.onPageChanged.bind(this)} currentPage={this.props.currentPage}
                           portionSize={10}/>
            </div>
        )
    };
}

export default Users;