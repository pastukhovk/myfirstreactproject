import {Field, Form, Formik} from "formik";
import React from "react";
import {UsersFilterType} from "../../redux/users-reducer";
import {useSelector} from "react-redux";
import {getUsersFilter} from "../../redux/users-selectors";

type UsersSearchFormType = {
    onFilterChanged: (filter: UsersFilterType) => void
}
type FormType = {
    term: string
    friend: 'null' | 'true' | 'false'
}
export const UsersSearchForm = (props: UsersSearchFormType) => {
    const filter = useSelector(getUsersFilter)
    return (
        <div>
            <Formik
                enableReinitialize
                initialValues={{term: filter.term, friend: String(filter.friend)}}
                onSubmit={(values, {setSubmitting}) => {
                    setTimeout(() => {
                        // alert(JSON.stringify(values, null, 2));
                        const filter: UsersFilterType = {
                            term: values.term,
                            friend: values.friend === 'null' ? null : values.friend === 'true' ? true : false
                        }
                        props.onFilterChanged(filter);
                        setSubmitting(false);
                    }, 400);
                }}
            >
                {({isSubmitting}) => (
                    <Form>
                        <Field type="text" name="term" placeholder="Please enter name"/>
                        <Field name="friend" as="select" placeholder="Select friends">
                            <option value="null">All</option>
                            <option value="true">Only followed</option>
                            <option value="false">Only unfollowed</option>
                        </Field>
                        <button type="submit" disabled={isSubmitting}>
                            Find
                        </button>
                    </Form>
                )}
            </Formik>
        </div>
    )
}