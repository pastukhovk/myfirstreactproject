import React from 'react';
import './users.scss'
import {NavLink} from "react-router-dom";
import userDefaultPicture from "./../../assets/images/no-user-photo.png";
import {UserType} from "../../redux/users-reducer";

type PropsType = {
    user: UserType
    followingIsProgress: Array<number>
    follow: (id: number) => void
    unfollow: (id: number) => void
    isAuth: boolean
}
let User: React.FC<PropsType> = ({user, followingIsProgress, follow, unfollow, isAuth}) => {
    let usersItemClassName = "users__item";
    usersItemClassName = isAuth ? usersItemClassName + " users__item_isAuth" : usersItemClassName;
    return (
        <>

            <div key={user.id} className={usersItemClassName}>

                <NavLink to={"/profile/" + user.id}>
                    <img className="avatar" alt="avatar"
                         src={(user.photos !== null) && (user.photos.small !== null) ? user.photos.small : userDefaultPicture}/>
                </NavLink>
                <div>{user.name}</div>
                <div>{user.id}</div>
                {
                    !isAuth ? null :
                        (user.followed) ?
                            <button className="btn" disabled={followingIsProgress.some(id => id === user.id)}
                                    onClick={() => {
                                        unfollow(user.id);
                                    }}> unfollow</button> :

                            <button className="btn" disabled={followingIsProgress.some(id => id === user.id)}
                                    onClick={() => {
                                        follow(user.id);
                                    }
                                    }>follow</button>

                }


                <div className="users__status">{user.status}</div>

            </div>

        </>
    );
};

export default User;