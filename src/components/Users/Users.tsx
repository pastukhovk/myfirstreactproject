import React, {FC, useEffect} from 'react';
import './users.scss';
import Preloader from "../common/Preloader/Preloader";
import {Paginator} from "../common/Paginator/Paginator";
import User from "./User";
import {
    followThunkCreator,
    getUsersThunkCreator,
    unfollowThunkCreator,
    UsersFilterType
} from "../../redux/users-reducer";
import {UsersSearchForm} from "./UsersSearchForm";
import {useDispatch, useSelector} from "react-redux";
import {
    getCurrentPage,
    getFollowingIsProgress,
    getIsAuth,
    getIsFetching,
    getPageSize,
    getTotalUsersCount,
    getUsers as getUserSelector,
    getUsersFilter
} from "../../redux/users-selectors"
import {useHistory} from 'react-router-dom';
import * as queryString from "querystring";

type QueryParamsType = { term?:string, currentPage?:string, friend?:string}
export const Users: FC = (props) => {
    const users = useSelector(getUserSelector)
    const pageSize = useSelector(getPageSize)
    const totalUsersCount = useSelector(getTotalUsersCount)
    const currentPage = useSelector(getCurrentPage)
    const isFetching = useSelector(getIsFetching)
    const followingIsProgress = useSelector(getFollowingIsProgress)
    const isAuth = useSelector(getIsAuth)
    const filter = useSelector(getUsersFilter)

    const dispatch = useDispatch()
    const history = useHistory()
    const getUsers = (pageSize: number, currentPage: number, filter: UsersFilterType) => {
        dispatch(getUsersThunkCreator(pageSize, currentPage, filter))
    }
    const follow = (id: number) => {
        dispatch(followThunkCreator(id))
    }
    const unfollow = (id: number) => {
        dispatch(unfollowThunkCreator(id))
    }

    useEffect(() => {
        //получаем query-параметры как свойства объекта и отсекаем ?
        const parsed = queryString.parse(history.location.search.substr(1));
        let actualPage = currentPage;
        let actualFilter = filter;
        if (parsed.page) {
            actualPage = Number(parsed.page)
        }
        switch (parsed.friends) {
            case "true" :
                actualFilter = {...actualFilter, friend: true}
                break;
            case "false" :
                actualFilter = {...actualFilter, friend: false}
                break;
            case "null":
                actualFilter = {...actualFilter, friend: null}
                break;
        }
        if (parsed.term) {
            actualFilter = {...actualFilter, term: String(parsed.term)}
        }
        getUsers(pageSize, actualPage, actualFilter);
    }, []); //Вызываем только 1 раз, поэтому в массиве зависимостей пустой массив.
    // Когда пользователь кликает по pageNumber осуществляем вызов в функции onPageChanged

    // синхронизация параметров фильтра и адресной строки браузера
    useEffect(() => {
        //создание объекта для автоматической генерации url
        const query:QueryParamsType = {}
        //проверка параметров фильтра
        if (!!filter.term) {
            query.term = filter.term
        }
        if (filter.friend !== null) {
            query.friend = String(filter.friend)
        }
        if (currentPage !== 1) {
            query.currentPage = String(currentPage)
        }
        history.push({
            pathname: '/users',
            search: queryString.stringify(query)
        })
    }, [filter, currentPage])

    let onPageChanged = (pageNumber: number) => {
        getUsers(pageSize, pageNumber, filter);
    }
    let onFilterChanged = (filter: UsersFilterType) => {
        getUsers(pageSize, 1, filter);
    }

    return (

        <div className="users">
            <UsersSearchForm onFilterChanged={onFilterChanged}/>
            {isFetching ? <Preloader/> : <h3>Зарегистрированные пользователи</h3>}

            {
                users.map(u =>
                    <User key={u.id} user={u} followingIsProgress={followingIsProgress}
                          follow={follow} unfollow={unfollow} isAuth={isAuth}
                    />
                )}
            {isFetching ? null : <Paginator totalUsersCount={totalUsersCount} pageSize={pageSize}
                                            onPageChanged={onPageChanged} currentPage={currentPage}
                                            portionSize={10}/>}
        </div>
    )

}