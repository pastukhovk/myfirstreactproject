import React from 'react';
import './header.scss';
import Header from "./Header";
import {connect} from "react-redux";
import {userAuthorized} from "../../redux/auth-reducer";
import {AppStateType} from "../../redux/redux-store";


class HeaderContainer extends React.Component<HeaderPropsType> {
    render() {
        return <Header isAuth = {this.props.isAuth} userAuthorized = {this.props.userAuthorized} login = {this.props.login} />
    }
}

type MapStateHeaderToPropsType = {
    isAuth: boolean
    login: string | null
}
type MapDispatchHeaderToPropsType = {
    userAuthorized: () => void
}
export type HeaderPropsType = MapStateHeaderToPropsType & MapDispatchHeaderToPropsType;
const mapStateToProps = (state: AppStateType):MapStateHeaderToPropsType => ({

    isAuth: state.auth.isAuth,
    login: state.auth.login,
});

export default connect<MapStateHeaderToPropsType, MapDispatchHeaderToPropsType, {}, AppStateType>(mapStateToProps, {userAuthorized})(HeaderContainer);