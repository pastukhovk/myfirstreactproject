import React from 'react';
import './header.scss';
import LogoutContainer from "../Login/Logout";
import {NavLink} from "react-router-dom";
import {HeaderPropsType} from "./HeaderContainer";
import socialNetwork from "./../../assets/images/socialNet.png";



const Header:React.FC<HeaderPropsType> = (props) => {
    return (

        <header>
            <div className="Name">
                <img src={socialNetwork} alt="icon"/>
            </div>
            <div className="Contacts">Friends Finder</div>

            {props.isAuth ?
                <div className="logout"><div className="logo">{props.login}</div> <LogoutContainer {...props} /> </div> :
              <div> <div className="logo">Вы не авторизованы</div> <div><button className="btn"><NavLink to={"/login"}>Войти</NavLink></button></div></div>
            }



        </header>
    );
};
export default Header;