import React from 'react';
import './navbar.scss';
import {NavLink} from "react-router-dom";
import SideBarFriendsContainer from "./SidebarFriends/SidebarFriendsContainer";




const Navbar = () => {

    return (
        <div className="navbar">
            <NavLink to="/profile">Профиль</NavLink>
            <NavLink to="/dialogs">Диалоги</NavLink>
            <NavLink to="/users">Пользователи</NavLink>
            <div className="sidebar">

                <SideBarFriendsContainer/>
            </div>
        </div>
    )
};

export default Navbar;