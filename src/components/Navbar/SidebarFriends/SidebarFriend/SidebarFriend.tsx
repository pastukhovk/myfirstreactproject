import React from 'react';
import "./sidebarfriend.scss"

type SidebarFriendPropsType = {
    key:number
    imageLink:string | null
    name:string
}
export const SidebarFriend = (props: SidebarFriendPropsType) => {
    return (
        <div className="SidebarFriend">
            {(props.imageLink) ? <img src={props.imageLink} alt="pic"/> : null}
            <span>{props.name}</span>
        </div>
    )

};