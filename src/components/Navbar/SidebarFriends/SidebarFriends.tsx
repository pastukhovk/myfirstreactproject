import React from 'react';
import {SidebarFriend} from "./SidebarFriend/SidebarFriend";
import "./sidebarfriends.scss"
import {InitialStateType} from "../../../redux/sidebar-reducer";


const SidebarFriends = (props: InitialStateType) => {
    let SidebarFriendElement = props.friends.map(friend => <SidebarFriend key={friend.id} imageLink={friend.imageLink}
                                                                          name={friend.name}/>)
    return (
        <div className="SBFriends">{SidebarFriendElement}</div>
    )

};

export default SidebarFriends;