import React from "react";
import {AppStateType} from "../../../redux/redux-store";
import {connect} from "react-redux";
import SidebarFriends from "./SidebarFriends";
import {compose} from "redux";

const mapStateToProps = (state: AppStateType) => ({
    friends: state.sidebar.friends
})
export default compose<React.ComponentType>(connect(mapStateToProps,null))(SidebarFriends)


