import React from "react";
import {connect} from "react-redux";
import {logoutThunkCreator} from "../../redux/auth-reducer";
type LogOutPropType = {
    logoutThunkCreator:() => void
}

export const Logout = (props:LogOutPropType) => {
    return (
    <div>
        <button className="btn" onClick={props.logoutThunkCreator}>Logout</button>
    </div>
    )
};

const LogoutContainer = connect(null,{logoutThunkCreator})(Logout);

export default LogoutContainer;