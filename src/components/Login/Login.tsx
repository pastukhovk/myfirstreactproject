import React from 'react';
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import {createField, FormsControlsCreator} from "../common/FormsControls/FormsControls";
import styles from "../common/FormsControls/FormControls.module.scss";
import {maxLength, required} from "../../helpers/validators";
import {connect} from "react-redux";
import {getCaptchaURL, loginThunkCreator} from "../../redux/auth-reducer";
import {Redirect} from "react-router-dom";
import {AppStateType} from "../../redux/redux-store";

const inputFormControls = FormsControlsCreator("input")
const maxLength30 = maxLength(30);

type MyOwnPropsType = {
    captchaUrl: string | null
    getCaptchaURL: () => void
}
const LoginForm:React.FC<InjectedFormProps<LoginFormValuesType,MyOwnPropsType> &  MyOwnPropsType> = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field component={inputFormControls} validate={[required, maxLength30]} type="text" name='login'
                       placeholder={"Enter Login"}/>
            </div>
            <div>
                <Field type='password' component={inputFormControls} validate={[required, maxLength30]} name='password'
                       placeholder={"Enter Password"}/>
            </div>
            <div>
                <label><Field component="input" name='rememberMe' type="checkbox"/> Remember me</label>
            </div>
            {props.error &&
            <div className={styles.formError}>{props.error}</div>
            }
            {props.captchaUrl && <div className="captchaBlock">
                <div><img alt="captcha" src={props.captchaUrl}/></div>
                <div> {createField("input", "checkCaptcha", "input", "", [required])} </div>
                <button onClick={props.getCaptchaURL}>Обновить каптчу</button>
            </div>
            }


            <div>
                <button type="submit">Send</button>
            </div>
        </form>
    )
};
const LoginReduxForm = reduxForm<LoginFormValuesType,MyOwnPropsType>({form: 'login'})(LoginForm);

type MapStateToPropsType = {
    isAuth: boolean
    captchaUrl: string | null
}

type MapDispatchToPropsType = {
    loginThunkCreator: (email: string, password: string, rememberMe: boolean, captchaUrl: string | null) => void
    getCaptchaURL: () => void
}

type LoginFormValuesType = {
    checkCaptcha: string | null
    rememberMe: boolean
    password: string
    login: string

}
const Login: React.FC<MapStateToPropsType & MapDispatchToPropsType> = (props) => {
    const onSubmitFunc = (data:LoginFormValuesType) => {
        props.loginThunkCreator(data.login, data.password, data.rememberMe, data.checkCaptcha)
    };
    if (props.isAuth) return <Redirect to={"/profile"}/>;
    return (
        <div>
            <h1>Авторизуйтесь</h1>
            <LoginReduxForm onSubmit={onSubmitFunc} captchaUrl={props.captchaUrl} getCaptchaURL={props.getCaptchaURL}/>
        </div>
    )
};
const mapStateToProps = (state: AppStateType): MapStateToPropsType => ({
    isAuth: state.auth.isAuth,
    captchaUrl: state.auth.captchaUrl
});
export const LoginContainer = connect(mapStateToProps, {loginThunkCreator, getCaptchaURL})(Login);