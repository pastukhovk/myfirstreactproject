import Dialogs from "./Dialogs";
import {connect} from "react-redux";
import {actions} from "../../redux/dialogs-reducer";
import {withAuthRedirect} from "../../hoc/withAuthRedirect";
import {compose} from "redux";
import {AppStateType} from "../../redux/redux-store";


let mapStateToProps = (state: AppStateType) => {
    return {state: state.dialogsPage};
};
const DialogsContainer = compose<React.ComponentType>(
    connect(mapStateToProps, {...actions}),
    withAuthRedirect
)(Dialogs);


export default DialogsContainer;