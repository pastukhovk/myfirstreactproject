import React from 'react';
import s from './dialogs.module.scss';
import {NavLink} from "react-router-dom";
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import {FormsControlsCreator} from "../common/FormsControls/FormsControls";
import {maxLength, required} from "../../helpers/validators";
import {InitialStateDialogPageType} from "../../redux/dialogs-reducer";


type DialogItemPropsType = {
    id: number
    name: string
}
type MessagePropsType = {
    message: string
}


const DialogItem: React.FC<DialogItemPropsType> = (props) => {
    return <div className="dialog"><NavLink to={"/dialogs/" + props.id}>{props.name}</NavLink></div>;
};

const Message: React.FC<MessagePropsType> = (props) => {
    return (
        <div className="message">{props.message}</div>
    )
};
const textAreaFormControls = FormsControlsCreator("textarea");
const maxLength100 = maxLength(100);
const AddMessageForm: React.FC<InjectedFormProps> = ({handleSubmit, reset}) => {

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <Field name={"message"} component={textAreaFormControls} validate={[required, maxLength100]} type="text"
                       placeholder="Enter your message"
                />
            </div>
            <div>
                <button type={"submit"} onClick={() => {
                    setTimeout(reset, 0)
                }}>Добавить
                </button>
            </div>
        </form>
    )
};
const AddMessageReduxForm = reduxForm({form: "userMessage"})(AddMessageForm);

type DialogsPropsType = {
    state: InitialStateDialogPageType
    addMessage: (message: string) => void
}

const Dialogs: React.FC<DialogsPropsType> = (props) => {

    let ContactsElements = props.state.ContactData.map(Contact => <DialogItem key={Contact.id} id={Contact.id}
                                                                              name={Contact.name}/>);
    let MessagesElements = props.state.MessageData.map(message => <Message key={message.id} message={message.text}/>);

    type addMessageTextPropsType = {
        data: object
    }
    const addMessageText = (data: any) => {
        props.addMessage(data.message);
    };
    return (
        <div className={s.dialogs}>

            <div className="dialogs-items">
                {ContactsElements}
            </div>

            <div className="messages">
                {MessagesElements}

                <AddMessageReduxForm onSubmit={addMessageText}/>

            </div>
        </div>
    )
};
export default Dialogs;